var data = [
  {
    jk : "wanita",
    pendidikan : "s2",
    bPekerjaan : "pendidikan",
    kUsia : "tua",
    stt : 1
  },
  {
    jk : "pria",
    pendidikan : "s1",
    bPekerjaan : "marketing",
    kUsia : "muda",
    stt : 1
  },
  {
    jk : "wanita",
    pendidikan : "sma",
    bPekerjaan : "wirausaha",
    kUsia : "tua",
    stt : 1
  },
  {
    jk : "pria",
    pendidikan : "s1",
    bPekerjaan : "profesional",
    kUsia : "tua",
    stt : 1
  },
  {
    jk : "pria",
    pendidikan : "s2",
    bPekerjaan : "profesional",
    kUsia : "muda",
    stt : 0
  },
  {
    jk : "pria",
    pendidikan : "sma",
    bPekerjaan : "wirausaha",
    kUsia : "muda",
    stt : 0
  },
  {
    jk : "wanita",
    pendidikan : "sma",
    bPekerjaan : "marketing",
    kUsia : "muda",
    stt : 0
  },
];

//data true
var hasTrue,hasJk,hasPen,hasPek,hasUsia,hasData;
var hasilAkhirTrue = 0;
var hasilAkhirFalse = 0;
var pA,p1,p2,p3,p4;

//data false
var hasFalse,hasJkFalse,hasPenFalse,hasPekFalse,hasUsiaFalse;
var pAFalse,p1False,p2False,p3False,p4False;
var akhirPisan;
var akhirPisan2;

//data dinamis
var JK,Pend,BP,KU;

function setup()
{
  hasTrue = sumTrue();
  hasFalse = sumFalse();
  hasData = data.length;

  //insialisasi
  JK = "wanita";
  Pend = "sma";
  BP = "pendidikan";
  KU = "tua";

  //prob yes
  hasJk = probJk(JK,1);
  hasPen = probPend(Pend,1);
  hasPek = probPek(BP,1)
  hasUsia = probUsia(KU,1);

  pA = hasTrue / hasData;
  p1 = hasJk / hasTrue;
  p2 = hasPen / hasTrue;
  p3 = hasPek / hasTrue;
  p4 = hasUsia / hasTrue;

  hasilAkhirTrue = pA * p1 * p2 * p3 * p4;

  //prob no
  hasJkFalse = probJk(JK,0);
  hasPenFalse = probPend(Pend,0);
  hasPekFalse = probPek(BP,0)
  hasUsiaFalse = probUsia(KU,0);

  pAFalse = hasFalse / hasData;
  p1False = hasJkFalse / hasFalse;
  p2False = hasPenFalse / hasFalse;
  p3False = hasPekFalse / hasFalse;
  p4False = hasUsiaFalse / hasFalse;

  hasilAkhirFalse = pAFalse * p1False * p2False * p3False * p4False;

  //running
  console.log("Jenis Kelamin : "+JK+"\nPendidikan : "+Pend+"\nPekerjaan : "+BP+"\nKategoria Usia : "+KU);
  console.log("\n");

  //detail
  console.log("pA "+hasTrue+"/"+hasData);
  console.log("PROB JK YES : "+hasJk+"/"+hasTrue);
  console.log("PROB PEND YES : "+hasPen+"/"+hasTrue);
  console.log("PROB PEKERJAAN YES : "+hasPek+"/"+hasTrue);
  console.log("PROB USIA YES : "+hasUsia+"/"+hasTrue);
  console.log("\n");
  console.log("pB "+hasFalse+"/"+hasData);
  console.log("PROB JK NO : "+hasJkFalse+"/"+hasFalse);
  console.log("PROB PEND NO : "+hasPenFalse+"/"+hasFalse);
  console.log("PROB PEKERJAAN NO : "+hasPekFalse+"/"+hasFalse);
  console.log("PROB USIA NO : "+hasUsiaFalse+"/"+hasFalse);

  console.log("\n");

  console.log("presentase yes : "+hasilAkhirTrue);
  console.log("presentase no : "+hasilAkhirFalse);

  //* *cek besar yes or no
  if(hasilAkhirFalse > hasilAkhirTrue){
    //* *hasil jika lebih besar no , no = no/yes dibagi yes + no
    akhirPisan = ( hasilAkhirFalse / (hasilAkhirTrue+hasilAkhirFalse) ) * 100;
    akhirPisan2 = 100 - akhirPisan;
    console.log("Lebih besar nilai false");
    console.log("presentasenya ditolak adalah : "+akhirPisan+"%");
    console.log("presentasenya diterima adalah : "+akhirPisan2+"%");
  }else if (hasilAkhirTrue > hasilAkhirFalse){
    akhirPisan = ( hasilAkhirTrue / (hasilAkhirTrue+hasilAkhirFalse) ) * 100;
    akhirPisan2 = 100 - akhirPisan;
    console.log("Lebih besar nilai true");
    console.log("presentasenya diterima adalah : "+akhirPisan+"%");
    console.log("presentasenya ditolak adalah : "+akhirPisan2+"%");
  }
  
}

//fungsi untuk mengecek jumlah yg true
function sumTrue()
{
  var has = 0;
  for(var i = 0 ; i < data.length ; i++)
  {
    if(data[i].stt == 1)
    {
      has += 1;
    }
  }

  return has;
}


//fungsi untuk mengecek jumlah yg false
function sumFalse()
{
  var has = 0;
  for(var i = 0 ; i < data.length ; i++)
  {
    if(data[i].stt == 0)
    {
      has += 1;
    }
  }

  return has;
}

function probJk(jk, bn)
{
  var has = 0;
  for(var i = 0 ; i < data.length ; i++)
  {
    if(data[i].jk == jk && data[i].stt ==  bn)
    {
      has += 1;
    }else if(data[i].jk == jk && data[i].stt ==  bn)
    {
      has += 1;
    }
  }

  return has;
}

function probPend(pend, bn)
{
  var has = 0;
  for(var i = 0 ; i < data.length ; i++)
  {
    if(data[i].pendidikan == pend && data[i].stt ==  bn)
    {
      has += 1;
    }else if(data[i].pendidikan == pend && data[i].stt ==  bn)
    {
      has += 1;
    }
  }

  return has;
}

function probPek(pek, bn)
{
  var has = 0;
  for(var i = 0 ; i < data.length ; i++)
  {
    if(data[i].bPekerjaan == pek && data[i].stt ==  bn)
    {
      has += 1;
    }else if(data[i].bPekerjaan == pek && data[i].stt ==  bn)
    {
      has += 1;
    }
  }

  return has;
}

function probUsia(usia, bn)
{
  var has = 0;
  for(var i = 0 ; i < data.length ; i++)
  {
    if(data[i].kUsia == usia && data[i].stt ==  bn)
    {
      has += 1;
    }else if(data[i].kUsia == usia && data[i].stt ==  bn)
    {
      has += 1;
    }
  }

  return has;
}

